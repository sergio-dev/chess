package chess.pieces;

import chess.GameState;
import chess.Player;
import chess.Position;

import java.util.List;

public abstract class RayMovingPiece extends Piece{

    protected RayMovingPiece(Player owner) {
        super(owner);
    }

    @Override
    public abstract List<Position> possibleMoves(Position position, GameState gameState);

    protected void stepsRay(char offsetX, int offsetY, Position curPos, GameState gameState, List<Position> newPositions ) {

    	char curX = curPos.getColumn();
    	int curY = curPos.getRow();
    	while ( true ){
    		curX += offsetX;
    		curY += offsetY;
    		if ( !Position.checkPosition(curX, curY)){
    			break;
    		}
            Position newPos = new Position((char)curX, curY);
            if (!process(gameState, newPos, newPositions)) {
                break;
            }
    	}
    }

    protected boolean process(GameState gameState, Position newPos, List<Position> newPositions) {
        Piece piece = gameState.getPieceAt(newPos);
        if (piece == null) {
            newPositions.add(newPos);
        } else {
            if ( piece.getOwner() != gameState.getCurrentPlayer() ) {
                newPositions.add(newPos);
            }
            return false;
        }
        return true;
    }

}
