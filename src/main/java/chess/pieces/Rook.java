package chess.pieces;

import chess.GameState;
import chess.Player;
import chess.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * The 'Rook' class
 */
public class Rook extends RayMovingPiece {

    public Rook(Player owner) {
        super(owner);
    }

    @Override
    protected char getIdentifyingCharacter() {
        return 'r';
    }

    @Override
    public List<Position> possibleMoves(Position curPos, GameState gameState) {
        List<Position> newPositions = new ArrayList<Position>();
        stepsRay((char)0, -1, curPos, gameState, newPositions);
        stepsRay((char)0, 1, curPos, gameState, newPositions);
        stepsRay((char)1, 0, curPos, gameState, newPositions);
        stepsRay((char)-1, 0, curPos, gameState, newPositions);
        return newPositions;
    }

}
