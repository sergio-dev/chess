package chess.pieces;

import chess.GameState;
import chess.Player;
import chess.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * The 'Bishop' class
 */
public class Bishop extends RayMovingPiece {


    public Bishop(Player owner) {
        super(owner);
    }

    @Override
    protected char getIdentifyingCharacter() {
        return 'b';
    }

    @Override
    public List<Position> possibleMoves(Position curPos, GameState gameState) {
        List<Position> newPositions = new ArrayList<Position>();
        stepsRay((char)1, -1, curPos, gameState, newPositions);
        stepsRay((char)-1, -1, curPos, gameState, newPositions);
        stepsRay((char)1, 1, curPos, gameState, newPositions);
        stepsRay((char)-1, 1, curPos, gameState, newPositions);
        return newPositions;
    }


}
