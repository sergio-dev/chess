package chess.pieces;

import chess.GameState;
import chess.Player;
import chess.Position;

import java.util.ArrayList;
import java.util.List;

public abstract class FixedMovingPiece extends Piece{

    protected FixedMovingPiece(Player owner) {
        super(owner);
    }

    protected abstract int[][] getSteps();

    @Override
    public List<Position> possibleMoves(Position curPos, GameState gameState) {
        List<Position> newPositions = new ArrayList<Position>();

        for (int[] step : getSteps()) {
            char char_ = (char) (((int) curPos.getColumn()) + step[0]);
            int number = curPos.getRow() + step[1];
            if (Position.checkPosition(char_, number)) {
                Position newPos = new Position(char_, number);
                Piece piece = gameState.getPieceAt(newPos);
                if (piece == null) {
                    newPositions.add(newPos);
                } else {
                    if ( piece.getOwner() != gameState.getCurrentPlayer() ) {
                        newPositions.add(newPos);
                    }
                }
            }
        }

        return newPositions;
    }
}
