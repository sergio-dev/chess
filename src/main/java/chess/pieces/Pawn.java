package chess.pieces;

import chess.GameState;
import chess.Player;
import chess.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * The Pawn
 */
public class Pawn extends Piece {
    public Pawn(Player owner) {
        super(owner);
    }

    @Override
    protected char getIdentifyingCharacter() {
        return 'p';
    }

    @Override
    public List<Position> possibleMoves(Position curPos, GameState gameState) {
        List<Position> newPositions = new ArrayList<Position>();
        if (getOwner() == Player.White) {

            move(gameState, newPositions, curPos, 1);

            char nextChar1 = (char) (curPos.getColumn()+1);
            attack(gameState, newPositions, nextChar1, curPos.getRow()+1);

            char nextChar2 = (char)(((int)curPos.getColumn())-1);
            attack(gameState, newPositions, nextChar2, curPos.getRow()+1);

        } else if(getOwner() == Player.Black) {

            move(gameState, newPositions, curPos, -1);

            char nextChar1 = (char)(((int)curPos.getColumn())+1);
            attack(gameState, newPositions, nextChar1, curPos.getRow()-1);

            char nextChar2 = (char)(((int)curPos.getColumn())-1);
            attack(gameState, newPositions, nextChar2, curPos.getRow()-1);

        }
        return newPositions;
    }

    private void attack(GameState gameState, List<Position> newPositions, char nextChar, int nextRow) {
        if (Position.checkPosition(nextChar, nextRow)) {
            Position attackPos1 = new Position(nextChar, nextRow);
            if (gameState.getPieceAt(attackPos1) != null && gameState.getPieceAt(attackPos1).getOwner() != gameState.getCurrentPlayer()) {
                newPositions.add(attackPos1);
            }
        }
    }

    private void move(GameState gameState, List<Position> newPositions, Position curPos, int k) {
        if (Position.checkPosition(curPos.getColumn(), curPos.getRow()+ (1 * k))) {
            Position newPos = new Position(curPos.getColumn(), curPos.getRow()+ (1 * k) );
            if (gameState.getPieceAt(newPos) == null) {
                newPositions.add(newPos);
                int startPos = 2;
                if (this.getOwner() == Player.Black) startPos = startPos + 5;
                if (curPos.getRow() == startPos) {
                    Position newPos2 = new Position(curPos.getColumn(), curPos.getRow()+ (2*k) );
                    if (gameState.getPieceAt(newPos2) == null) {
                        newPositions.add(newPos2);
                    }
                }
            }
        }
    }

}
