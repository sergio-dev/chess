package chess.pieces;

import chess.Player;

/**
 * The Knight class
 */
public class Knight extends FixedMovingPiece {

    private int[][] steps = {{1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}, {-2, 1}, {-1, 2}};

    public Knight(Player owner) {
        super(owner);
    }

    @Override
    protected char getIdentifyingCharacter() {
        return 'n';
    }

    @Override
    protected int[][] getSteps() {
        return this.steps;
    }

}
