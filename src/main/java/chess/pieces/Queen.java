package chess.pieces;

import chess.GameState;
import chess.Player;
import chess.Position;

import java.util.List;

/**
 * The Queen
 */
public class Queen extends Piece{
    public Queen(Player owner) {
        super(owner);
    }

    @Override
    protected char getIdentifyingCharacter() {
        return 'q';
    }

    @Override
    public List<Position> possibleMoves(Position position, GameState gameState) {

        Player player = gameState.getCurrentPlayer();

        Rook rook = new Rook(player);
        List<Position> rookPosition = rook.possibleMoves(position, gameState);

        Bishop bishop = new Bishop(player);
        List<Position> bishopPosition = bishop.possibleMoves(position, gameState);
        rookPosition.addAll(bishopPosition);

        return rookPosition;
    }
}
