package chess.pieces;

import chess.Player;

/**
 * The King class
 */
public class King extends FixedMovingPiece {

    private int[][] steps = { {0,1}, {1,1}, {1,0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}};

    public King(Player owner) {
        super(owner);
    }

    @Override
    protected char getIdentifyingCharacter() {
        return 'k';
    }

    @Override
    protected int[][] getSteps() {
        return this.steps;
    }
}
