package chess;


import chess.pieces.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that represents the current state of the game.  Basically, what pieces are in which positions on the
 * board.
 */
public class GameState {

    /**
     * The current player
     */
    private Player currentPlayer = Player.White;

    /**
     * A map of board positions to pieces at that position
     */
    private Map<Position, Piece> positionToPieceMap;

    /**
     * Create the game state.
     */
    public GameState() {
        positionToPieceMap = new HashMap<Position, Piece>();
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Call to initialize the game state into the starting positions
     */
    public void reset() {

        // White Pieces
        placePiece(new Rook(Player.White), new Position("a1"));
        placePiece(new Knight(Player.White), new Position("b1"));
        placePiece(new Bishop(Player.White), new Position("c1"));
        placePiece(new Queen(Player.White), new Position("d1"));
        placePiece(new King(Player.White), new Position("e1"));
        placePiece(new Bishop(Player.White), new Position("f1"));
        placePiece(new Knight(Player.White), new Position("g1"));
        placePiece(new Rook(Player.White), new Position("h1"));
        placePiece(new Pawn(Player.White), new Position("a2"));
        placePiece(new Pawn(Player.White), new Position("b2"));
        placePiece(new Pawn(Player.White), new Position("c2"));
        placePiece(new Pawn(Player.White), new Position("d2"));
        placePiece(new Pawn(Player.White), new Position("e2"));
        placePiece(new Pawn(Player.White), new Position("f2"));
        placePiece(new Pawn(Player.White), new Position("g2"));
        placePiece(new Pawn(Player.White), new Position("h2"));

        // Black Pieces
        placePiece(new Rook(Player.Black), new Position("a8"));
        placePiece(new Knight(Player.Black), new Position("b8"));
        placePiece(new Bishop(Player.Black), new Position("c8"));
        placePiece(new Queen(Player.Black), new Position("d8"));
        placePiece(new King(Player.Black), new Position("e8"));
        placePiece(new Bishop(Player.Black), new Position("f8"));
        placePiece(new Knight(Player.Black), new Position("g8"));
        placePiece(new Rook(Player.Black), new Position("h8"));
        placePiece(new Pawn(Player.Black), new Position("a7"));
        placePiece(new Pawn(Player.Black), new Position("b7"));
        placePiece(new Pawn(Player.Black), new Position("c7"));
        placePiece(new Pawn(Player.Black), new Position("d7"));
        placePiece(new Pawn(Player.Black), new Position("e7"));
        placePiece(new Pawn(Player.Black), new Position("f7"));
        placePiece(new Pawn(Player.Black), new Position("g7"));
        placePiece(new Pawn(Player.Black), new Position("h7"));
    }

    /**
     * Get the piece at the position specified by the String
     * @param colrow The string indication of position; i.e. "d5"
     * @return The piece at that position, or null if it does not exist.
     */
    public Piece getPieceAt(String colrow) {
        Position position = new Position(colrow);
        return getPieceAt(position);
    }

    /**
     * Get the piece at a given position on the board
     * @param position The position to inquire about.
     * @return The piece at that position, or null if it does not exist.
     */
    public Piece getPieceAt(Position position) {
        return positionToPieceMap.get(position);
    }

    /**
     * Method to place a piece at a given position
     * @param piece The piece to place
     * @param position The position
     */
    protected void placePiece(Piece piece, Position position) {
        positionToPieceMap.put(position, piece);
    }

    public Map<Position, List<Position>> getPossibleMoves() {
        Map<Position, List<Position>> moves = getDirtyPossibleMoves();
        Player curPlayer = currentPlayer;
        for (Map.Entry<Position, List<Position>> m  :  moves.entrySet()) {
            Position oldPos = m.getKey();
            List<Position> uncheckedPos = m.getValue();
            List<Position> unallowedPos = new ArrayList<Position>();
            for (Position newPos : uncheckedPos) {
                Piece oldPiece = getPieceAt(oldPos);
                Piece newPiece = getPieceAt(newPos);
                makeMove(oldPos, null, newPos, oldPiece);
                Position kingPosition = getPositionByKing(curPlayer);
                Map<Position, List<Position>> enemyMoving = getDirtyPossibleMoves();
                for (List<Position> p :  enemyMoving.values() ){
                    for (Position p1 : p) {
                        if (p1.equals(kingPosition)) {
                            unallowedPos.add(newPos);
                        }
                    }
                }
                makeMove(oldPos, oldPiece, newPos, newPiece); // rollback
            }
            uncheckedPos.removeAll(unallowedPos);
        }
        return moves;
    }

    private void makeMove(Position oldPos, Piece oldPiece, Position newPos, Piece newPiece) {
        placePiece(oldPiece, oldPos);
        placePiece(newPiece, newPos);
        switchCurrentPlayer();
    }

    protected void switchCurrentPlayer() {
        if (currentPlayer == Player.Black)
            currentPlayer = Player.White;
        else currentPlayer = Player.Black;
    }

    private Map<Position, List<Position>> getDirtyPossibleMoves() {
        Map<Position, List<Position>> moving = new HashMap<Position, List<Position>>();
        for (int x = Position.MIN_ROW; x <= Position.MAX_ROW; x++) {
            for (int y = Position.MIN_COLUMN; y <= (int)Position.MAX_COLUMN; y++) {
                String curPosStr = Character.toString((char)y) + x;
                Position curPosObj = new Position(curPosStr);
                Piece piece = getPieceAt(curPosObj);
                if (piece != null && piece.getOwner() == getCurrentPlayer()) {
                    List<Position> positions = piece.possibleMoves(curPosObj, this);
                    if (positions == null) continue;
                    moving.put(curPosObj, positions);
                }
            }
        }
        return moving;
    }

    public Position getPositionByKing(Player player) {
        for (int x = Position.MIN_ROW; x <= Position.MAX_ROW; x++) {
            for (int y = Position.MIN_COLUMN; y <= (int) Position.MAX_COLUMN; y++) {
                String curPosStr = Character.toString((char)y) + x;
                Position curPosObj = new Position(curPosStr);
                Piece piece = getPieceAt(curPosObj);
                if ((piece != null && piece.getOwner() == player) &&
                        (piece.getIdentifier() == 'k' || piece.getIdentifier() == 'K')) {
                    return curPosObj;
                }
            }
        }
        return null;
    }

    public boolean moving(Position startPos, Position endPos) {
        boolean res = false;
        Map<Position, List<Position>> moves = getPossibleMoves();
        if (moves != null) {
            List<Position> positions = moves.get(startPos);
            if (positions != null && positions.contains(endPos)) {
                makeMove(startPos, null, endPos, getPieceAt(startPos));
                res = true;
            }
        }
        return res;
    }

    public boolean isDraw() {
        for (Map.Entry<Position, List<Position>> pos :  getPossibleMoves().entrySet() ) {
            if (pos.getValue() != null && pos.getValue().size() > 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isCheckKing() {
        Position kingPos = getPositionByKing(currentPlayer);
        switchCurrentPlayer();
        for (Map.Entry<Position, List<Position>> pos :  getPossibleMoves().entrySet() ) {
            if (pos.getValue().contains(kingPos)) {
                switchCurrentPlayer();
                return true;
            }
        }
        switchCurrentPlayer();
        return false;
    }

    public boolean isMate() {
        Position kingPos = getPositionByKing(currentPlayer);
        switchCurrentPlayer();
        for (Map.Entry<Position, List<Position>> pos :  getPossibleMoves().entrySet() ) {
            if (pos.getValue().contains(kingPos)) {
                switchCurrentPlayer();
                getPossibleMoves().entrySet();
                for (Map.Entry<Position, List<Position>> pos1 :  getPossibleMoves().entrySet() ) {
                    if (pos1.getValue() != null && pos1.getValue().size() > 0) {
                        return false;
                    }
                }
                return true;
            }
        }
        switchCurrentPlayer();
        return false;
    }
}
