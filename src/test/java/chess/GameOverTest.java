package chess;

import chess.pieces.King;
import chess.pieces.Queen;
import chess.pieces.Rook;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class GameOverTest {
    @Test
    public void testDraw() {

        TestGameState gameState = new TestGameState();

        gameState.placePiece(new Queen(Player.White), new Position("c6"));
        gameState.placePiece(new King(Player.White), new Position("f6"));

        gameState.placePiece(new King(Player.Black), new Position("d8"));
        gameState.switchCurrentPlayer();

        assertEquals("is it Draw?", true, gameState.isDraw());

    }

    @Test
    public void testMate() {

        TestGameState gameState = new TestGameState();

        gameState.placePiece(new Rook(Player.Black), new Position("d8"));
        gameState.placePiece(new Rook(Player.Black), new Position("f8"));
        gameState.placePiece(new King(Player.Black), new Position("e8"));

        gameState.placePiece(new Queen(Player.White), new Position("e6"));
        gameState.placePiece(new King(Player.White), new Position("g1"));

        gameState.switchCurrentPlayer();
        assertEquals("is it Mate?", true, gameState.isMate());
    }

    @Test
    public void testCheckMate() {

        TestGameState gameState = new TestGameState();

        gameState.placePiece(new King(Player.Black), new Position("h5"));

        gameState.placePiece(new Rook(Player.White), new Position("h1"));
        gameState.placePiece(new King(Player.White), new Position("f4"));

        gameState.switchCurrentPlayer();

        assertEquals("is it checkmate?", true, gameState.isCheckKing());
    }
}
